package protocol.response;

import hello.Employee;

public class EloginResponse {
	private String text;
	private Employee employee;
	public EloginResponse(String text) {
		super();
		this.text = text;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee user) {
		this.employee = user;
	}
	public static EloginResponse OK() {
		return new EloginResponse("Login Successfull!");
	}

	@Override
	public String toString() {
		return "EloginResponse [text=" + text + ", employee=" + employee + "]";
	}
	public static EloginResponse NO_User() {
		return new EloginResponse("User not found!");
	}

	public static LoginResponse ERROR() {
		return new LoginResponse("ERROR!");
	}
	
	public static LoginResponse WRONG_PASSWORD() {
		return new LoginResponse("Wrong Password!");
	}
}
