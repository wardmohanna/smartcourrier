package protocol.response;

import hello.Courier;

public class LoginResponse {
	private String text;
	private Courier courier;
	public LoginResponse(String text) {
		super();
		this.text = text;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Courier getCourier() {
		return courier;
	}
	public void setCourier(Courier user) {
		this.courier = user;
	}
	public static LoginResponse OK() {
		return new LoginResponse("Login Successfull!");
	}

	public static LoginResponse NO_User() {
		return new LoginResponse("User not found!");
	}

	public static LoginResponse ERROR() {
		return new LoginResponse("ERROR!");
	}
	
	public static EloginResponse WRONG_PASSWORD() {
		return new EloginResponse("Wrong Password!");
	}
}
