package protocol.response;

import java.util.ArrayList;

import hello.Shipment;

public class RegionResponse {
	String text;
	private ArrayList<Shipment> shipments;
	public RegionResponse(String text) {
		super();
		this.text = text;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public ArrayList<Shipment> getShipments() {
		return shipments;
	}
	public void setShipments(ArrayList<Shipment> shipments) {
		this.shipments = shipments;
	}
	public static RegionResponse NO_Shipment() {
		return new RegionResponse("No shipments found!");
	}
	public static RegionResponse OK() {
		return new RegionResponse("OK");
	}
	
}
