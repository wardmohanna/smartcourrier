package ServerGUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Eshipments {

	private JFrame shipmentF;

	/**
	 * Launch the application.
	 */
	public static void ShipmentsFrame() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eshipments window = new Eshipments();
					window.shipmentF.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Eshipments() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		shipmentF = new JFrame();
		shipmentF.setBounds(1100, 500, 1600, 695);
		shipmentF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		shipmentF.getContentPane().setLayout(null);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				 ELogin.LoginFrame();
				 shipmentF.setVisible(false);
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnExit.setBounds(1371, 496, 171, 83);
		shipmentF.getContentPane().add(btnExit);
		
		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				 EHome.HomeFrame();
				 shipmentF.setVisible(false);
			}
		});
		btnBack.setBounds(1371, 410, 171, 75);
		shipmentF.getContentPane().add(btnBack);
	}
}
