package ServerGUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Image;
import java.awt.SystemColor;
import javax.swing.JLabel;

public class EHome {

	private JFrame HomeF;

	/**
	 * Launch the application.
	 */
	public static void HomeFrame() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EHome window = new EHome();
					window.HomeF.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EHome() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		HomeF = new JFrame();
		HomeF.setBounds(1100, 500, 1605, 768);
		HomeF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		HomeF.getContentPane().setLayout(null);
		
	
		JButton btnGenerateA = new JButton("Generate Redistribution ");
		btnGenerateA.setFont(new Font("Tahoma", Font.BOLD, 34));
		Image image = new ImageIcon("pictures/run.png").getImage();
		btnGenerateA.setIcon(new ImageIcon(image));
		btnGenerateA.setBackground(SystemColor.activeCaption);
		btnGenerateA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnGenerateA.setBounds(398, 139, 750, 150);
		HomeF.getContentPane().add(btnGenerateA);
		
		JButton btnNewshipment = new JButton("      Add New Shipment");
		btnNewshipment.setFont(new Font("Tahoma", Font.BOLD, 34));
		btnNewshipment.setBackground(SystemColor.activeCaption);
		 image = new ImageIcon("pictures/add.png").getImage();
		 btnNewshipment.setIcon(new ImageIcon(image));
		btnNewshipment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Eshipments.ShipmentsFrame();
				HomeF.setVisible(false);
			}
		});
		btnNewshipment.setBounds(398, 317, 750, 150);
		HomeF.getContentPane().add(btnNewshipment);
		
		JButton btnExit = new JButton("Exit");
		image = new ImageIcon("pictures/logout.png").getImage();
		btnExit.setIcon(new ImageIcon(image));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				 ELogin.LoginFrame();
				 HomeF.setVisible(false);
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnExit.setBounds(1376, 481, 171, 83);
		HomeF.getContentPane().add(btnExit);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setBounds(48, 58, 237, 243);
		image = new ImageIcon("pictures/logo.png").getImage();
		lblLogo.setIcon(new ImageIcon(image));
		HomeF.getContentPane().add(lblLogo);
		
		JLabel lblBg = new JLabel("");
		lblBg.setBounds(0, 0, 1600, 695);
		image = new ImageIcon("pictures/back.jpg").getImage();
		lblBg.setIcon(new ImageIcon(image));
		HomeF.getContentPane().add(lblBg);
	}
}
