package ServerGUI;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hello.Application;
import hello.EloginController;
import protocol.response.EloginResponse;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.Console;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.Color;
import java.awt.SystemColor;

public class ELogin {

	private JFrame EloginFrame;
	private JTextField txtId;
	private JPasswordField txtpassword;
	private static final Logger log = LoggerFactory.getLogger(Application.class);
	/**
	 * Launch the application.
	 */
	public static void LoginFrame() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ELogin window = new ELogin();
					window.EloginFrame.setVisible(true);
									} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ELogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("deprecation")
	private void initialize() {
		EloginFrame = new JFrame();
		EloginFrame.setBounds(1100, 500, 1500, 900);
		EloginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		EloginFrame.getContentPane().setLayout(null);
		
		
	
		txtId = new JTextField();
		txtId.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				  char c = e.getKeyChar();
				  if (c <'0' || c>'9'   )
				     e.consume();  // ignore event
				}});

		txtId.setFont(new Font("Tahoma", Font.PLAIN, 32));
		txtId.setBounds(1024, 462, 236, 39);
		EloginFrame.getContentPane().add(txtId);
		txtId.setColumns(10);
		
		
		JLabel lblId = new JLabel("ID : ");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 29));
		lblId.setBounds(945, 450, 66, 60);
		EloginFrame.getContentPane().add(lblId);
		
		JLabel lblPassword = new JLabel("Password : ");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 29));
		lblPassword.setBounds(854, 504, 156, 47);
		EloginFrame.getContentPane().add(lblPassword);
		
		txtpassword = new JPasswordField();
		txtpassword.setFont(new Font("Tahoma", Font.PLAIN, 32));
		txtpassword.setBounds(1024, 509, 236, 39);
		txtpassword.setColumns(10);
		EloginFrame.getContentPane().add(txtpassword);
		
		JLabel lblError = new JLabel("* error");
		lblError.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblError.setForeground(Color.RED);
		lblError.setBounds(1024, 549, 339, 33);
		EloginFrame.getContentPane().add(lblError);
		lblError.hide();
		
		JButton btnSignIn = new JButton("Sign in");
		btnSignIn.setBackground(SystemColor.activeCaption);
		btnSignIn.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent arg0) {

				

				String id  = txtId.getText();
				String password = txtpassword.getText();
				EloginController elogin = new EloginController();
				log.info("Employee login response" + " " +elogin.login(id, password).getText() );
				if (id=="")
				{
					lblError.setText("* Please fill an ID");
					lblError.show();
				}
				else if (password=="")
				{
					lblError.setText("* Please fill a password");
					lblError.show();
				}
				else if(elogin.login(id, password).getText() == EloginResponse.OK().getText())
				{
					lblError.setText("");
					lblError.hide();
					 EHome.HomeFrame();
					 EloginFrame.setVisible(false);
				}
				else if(elogin.login(id, password).getText() == EloginResponse.NO_User().getText())
				{
					lblError.setText("* User Not Found");
					lblError.show();
				}
				else if(elogin.login(id, password).getText() == EloginResponse.WRONG_PASSWORD().getText())
				{
					lblError.setText("* Wrong Password");
					lblError.show();
				}
			}
		});
		btnSignIn.setFont(new Font("Tahoma", Font.BOLD, 32));
		btnSignIn.setBounds(1024, 582, 236, 47);
		EloginFrame.getContentPane().add(btnSignIn);
		
		JLabel background=new JLabel();
		///Image image = Toolkit.getDefaultToolkit().getImage("/pictures/BG.jpg");
		Image image = new ImageIcon("pictures/BG.jpg").getImage();
		background.setIcon(new ImageIcon(image));
		
		background.setBounds(0, 0, 1500, 900);
		EloginFrame.getContentPane().add(background);
		
		
	}
}
