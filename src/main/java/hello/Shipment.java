package hello;

public class Shipment {
	private String id;
	private String address;
	private String area;
	private String status;
	private String courierID;
	private String imagePath;
	private String text;
	private String urgent;
	
	public Shipment(String id, String address, String area, String status, String courierID, String imagePath,
			String text, String urgent) {
		super();
		this.id = id;
		this.address = address;
		this.area = area;
		this.status = status;
		this.courierID = courierID;
		this.imagePath = imagePath;
		this.text = text;
		this.urgent = urgent;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCourierID() {
		return courierID;
	}
	public void setCourierID(String courierID) {
		this.courierID = courierID;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUrgent() {
		return urgent;
	}
	public void setUrgent(String urgent) {
		this.urgent = urgent;
	}
	@Override
	public String toString() {
		return "Shipment [id=" + id + ", address=" + address + ", area=" + area + ", status=" + status + ", courierID="
				+ courierID + ", imagePath=" + imagePath + ", text=" + text + ", urgent=" + urgent + "]";
	}
	
}
