package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import protocol.response.RegionResponse;

@RestController
@RequestMapping("/getRegion")
public class RegionController {
	private static final Logger log = LoggerFactory.getLogger(Application.class);
    
    @RequestMapping("")
    public RegionResponse getRegion(@RequestParam(value="region", defaultValue=" ") String region) {
    	log.info("SERVER: HTTP:getRegion REQUEST for: "+ region);
    	return DBLayer.getInstance().getRegionShipments(region);
       }
    
}
