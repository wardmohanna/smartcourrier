package hello;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greeting")
public class GreetingController {
	private static final Logger log = LoggerFactory.getLogger(Application.class);
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    
    
    @RequestMapping("")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
    	log.info("greetings request for: "+ name);
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
}
