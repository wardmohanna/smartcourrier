package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import protocol.response.EloginResponse;


@RestController
@RequestMapping("/Elogin")
public class EloginController {
	private static final Logger log = LoggerFactory.getLogger(Application.class);
    
    @RequestMapping("")
    public EloginResponse login(@RequestParam(value="id", defaultValue="0000") String id,@RequestParam(value="pass", defaultValue="0000") String pass) {
    	log.info("SERVER: HTTP:CheckEmployee REQUEST for: "+ id);
    	return DBLayer.getInstance().EisUser(id, pass);
       }
    
}
