package hello;

public class Courier {
	private String id;
	private String password;
	private String firstName,lastName;
	private String address;
	private String telephone;
	public Courier(String id, String password) {
		super();
		this.id = id;
		this.password = password;
	}
	public Courier(String id, String password, String firstName, String lastName, String address, String telephone) {
		super();
		this.id = id;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.telephone = telephone;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	@Override
	public String toString() {
		return "Courier [id=" + id + ", password=" + password + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", address=" + address + ", telephone=" + telephone + "]";
	}
	
}
