package hello;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import protocol.response.EloginResponse;
import protocol.response.LoginResponse;
import protocol.response.RegionResponse;



/**
 * DBLayer is a singelton class which would connect to the DataBase and work with it
 * @author wardm
 *
 */
 
public class DBLayer {
	private static DBLayer instance = null;
	private static final Logger log = LoggerFactory.getLogger(Application.class);
    private JdbcTemplate jdbcTemplate;
	
	private DBLayer(JdbcTemplate jdbcTemplate) {
			this.jdbcTemplate=jdbcTemplate;
		   log.info("Checking if tables exists, if not we create them.");
		   jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS couriers(" +
                   "id VARCHAR(9) PRIMARY KEY, password VARCHAR(255), first_name VARCHAR(255)," +
	                 "last_name VARCHAR(255), address VARCHAR(255), telephone VARCHAR(255))");
		   // table for shipments
		   jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS shipments(" +
	                            "id SERIAL PRIMARY KEY, address VARCHAR(255), area VARCHAR(255)," +
				                "status VARCHAR(1),courier_id VARCHAR(9), FOREIGN KEY(courier_id) REFERENCES couriers(id)," +
	                            "image_path VARCHAR(255), text VARCHAR(255), urgent VARCHAR(1))");
		   // Table for company employees to login
		   jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS employees(" +
                   "id VARCHAR(9) PRIMARY KEY, password VARCHAR(255), first_name VARCHAR(255)," +
	                 "last_name VARCHAR(255), address VARCHAR(255), telephone VARCHAR(255))");  
		   log.info("Tables Built!");
	   }
	public static void newInstance(JdbcTemplate jdbcTemplate) {
		instance= new DBLayer(jdbcTemplate);
	}
	public static DBLayer getInstance() {
	      return instance;
	   }
	   
	   public LoginResponse isUser(String id, String pass)  {
		   ArrayList<Courier> couriers = new ArrayList<Courier>();
		   log.info("isUser called!");
		// jdbcTemplate.batchUpdate("INSERT INTO Couriers VALUES ('200388353', '200388353', 'Raghda','Kais', 'Abo-Snan', '0507777777'); ");
		   jdbcTemplate.query(
	                "SELECT id, password, first_name, last_name, address, telephone FROM couriers WHERE id = ?",new Object[] { id.toString() },
	                (rs, rowNum) -> new Courier(rs.getString(1), rs.getString(2), rs.getString(3),
	                							rs.getString(4),rs.getString(5),rs.getString(6))
	        ).forEach(courier -> couriers.add(courier));
		   if(couriers.isEmpty()) {
			   return LoginResponse.NO_User();
		   }
		   Courier courier = couriers.get(0);
		   if(courier.getPassword().equals(pass)) {
				   LoginResponse response = LoginResponse.OK();
				   response.setCourier(courier);
				   log.info(courier.toString());
				   return response; 
			   }
		   else {
			   return EloginResponse.WRONG_PASSWORD();
		   }
		}
	   
	   
	   public EloginResponse EisUser(String id, String pass)  {
		   ArrayList<Employee> employees = new ArrayList<Employee>();
		   log.info("EisUser called!");
		   jdbcTemplate.query(
	                "SELECT id, password, first_name, last_name, address, telephone FROM employees WHERE id = ?",new Object[] { id.toString() },
	                (rs, rowNum) -> new Employee(rs.getString(1), rs.getString(2), rs.getString(3),
	                							rs.getString(4),rs.getString(5),rs.getString(6))
	        ).forEach(employee -> employees.add(employee));
		   if(employees.isEmpty()) {
			   return EloginResponse.NO_User();
		   }
		   else
		   {
			   Employee employee = employees.get(0);
			   if(employee.getPassword().equals(pass)) {
				   EloginResponse response = EloginResponse.OK();
				   response.setEmployee(employee);
				   log.info("response" + employee.toString());
				   return response; 
			   	}
			   else {
				   return LoginResponse.WRONG_PASSWORD();
			   }
		   }
		}
	   
	   public RegionResponse getRegionShipments(String region) {
		   ArrayList<Shipment> shipments = new ArrayList<Shipment>();
		   log.info("getRegionShipments called!");
		   jdbcTemplate.query(
	                "SELECT id, address, area, status, courier_id, image_path, text, urgent FROM shipments WHERE status = '0' AND area = ?",new Object[] { region.toString() },
	                (rs, rowNum) -> new Shipment(rs.getString(1), rs.getString(2), rs.getString(3),
	                							rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8))
	        ).forEach(shipment -> shipments.add(shipment));
		   if(shipments.isEmpty()) {
			   return RegionResponse.NO_Shipment();
		   }
		   RegionResponse response = RegionResponse.OK();
		   response.setShipments(shipments);
		   log.info("Shipments = "+shipments.toString());
		   return response;
	   }
	   
}
