package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import protocol.response.LoginResponse;

@RestController
@RequestMapping("/login")
public class LoginController {
	private static final Logger log = LoggerFactory.getLogger(Application.class);
    
    @RequestMapping("")
    public LoginResponse login(@RequestParam(value="id", defaultValue="0000") String id,@RequestParam(value="pass", defaultValue="0000") String pass) {
    	log.info("SERVER: HTTP:CheckCourier REQUEST for: "+ id);
    	return DBLayer.getInstance().isUser(id, pass);
       }
    
}
