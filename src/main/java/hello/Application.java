package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;



import ServerGUI.ELogin;


@SpringBootApplication
public class Application implements CommandLineRunner{
	
	private static final Logger log = LoggerFactory.getLogger(Application.class);
	
    public static void main(String[] args) {	
        final String dir = System.getProperty("user.dir");
    	////log.info("Direction is : "+ getClass().getResource());
    	 System.setProperty("java.awt.headless", "false"); 
        SpringApplication.run(Application.class, args);
    	

        

    }
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Override
    public void run(String... strings) throws Exception {
    	DBLayer.newInstance(jdbcTemplate);
		log.info("DBLayer instance created!");
     	 ELogin.LoginFrame();
	
    }
    
}